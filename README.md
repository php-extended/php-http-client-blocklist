# php-extended/php-http-client-blocklist

A psr-18 compliant middleware client that handles domain-based blocklists

![coverage](https://gitlab.com/php-extended/php-http-client-blocklist/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-blocklist/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-blocklist ^8`


## Basic Usage

This library is to make a man in the middle for http request and response and 
deny all the requests that does to unacceptable or unsafe domains.

```php

/* @var $client  \Psr\Http\Client\ClientInterface   */ // psr-18
/* @var $request \Psr\Http\Message\RequestInterface */ // psr-7
/* @var $responseFactory \Psr\Http\Message\ResponseFactoryInterface */ // psr-17
/* @var $blocklist \PhpExtended\Blocklist\BlocklistInterface */

$client = new BlocklistClient($client, $responseFactory, $blocklist);
$response = $client->sendRequest($request);

/* @var $response \Psr\Http\Message\ResponseInterface */
// this returns a 403 if the request was denied
// this forwards the request to the real client if allowed

```


## License

MIT (See [license file](LICENSE)).
