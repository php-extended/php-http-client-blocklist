<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-blocklist library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Blocklist\BlocklistInterface;
use PhpExtended\HttpClient\BlocklistClient;
use PhpExtended\HttpClient\BlocklistConfiguration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseFactoryInterface;

/**
 * BlocklistClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\BlocklistClient
 *
 * @internal
 *
 * @small
 */
class BlocklistClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BlocklistClient
	 */
	protected BlocklistClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BlocklistClient(
			$this->getMockForAbstractClass(ClientInterface::class),
			$this->getMockForAbstractClass(ResponseFactoryInterface::class),
			$this->getMockForAbstractClass(BlocklistInterface::class),
			new BlocklistConfiguration(),
		);
	}
	
}
