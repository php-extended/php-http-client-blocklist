<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-blocklist library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use ArrayIterator;
use Iterator;
use Psr\Http\Message\UriInterface;
use Stringable;

\defined('IDNA_DEFAULT') || \define('IDNA_DEFAULT', 0);
\defined('INTL_IDNA_VARIANT_UTS46') || \define('INTL_IDNA_VARIANT_UTS46', 1);

/**
 * BlocklistConfiguration class file.
 * 
 * This class represents the configuration for the blocklist client.
 * 
 * @author Anastaszor
 */
class BlocklistConfiguration implements Stringable
{
	
	/**
	 * Gets whether the blocklist should block every domain except the domains
	 * that are referenced in the whitelist.
	 * 
	 * @var boolean
	 */
	protected bool $_blockAllNotWhlst = false;
	
	/**
	 * Gets the whitelisted domains.
	 * 
	 * @var array<string, int>
	 */
	protected array $_whitelist = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Disables to block everyting except whitelist.
	 * 
	 * @return BlocklistConfiguration
	 */
	public function disableBlockAllExceptWhitelist() : BlocklistConfiguration
	{
		$this->_blockAllNotWhlst = false;
		
		return $this;
	}
	
	/**
	 * Enables to block everything except whitelist.
	 * 
	 * @return BlocklistConfiguration
	 */
	public function enableBlockAllExceptWhitelist() : BlocklistConfiguration
	{
		$this->_blockAllNotWhlst = true;
		
		return $this;
	}
	
	/**
	 * Gets whether to block everything except whitelist.
	 * 
	 * @return boolean
	 */
	public function hasBlockAllExceptWhitelist() : bool
	{
		return $this->_blockAllNotWhlst;
	}
	
	/**
	 * Adds a given domain to the whitelist.
	 * 
	 * @param string $domain
	 * @return BlocklistConfiguration
	 */
	public function addWhitelistDomain(string $domain) : BlocklistConfiguration
	{
		if(\function_exists('idn_to_ascii'))
		{
			$domain = \idn_to_ascii($domain, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
		}
		if(false !== $domain)
		{
			$this->_whitelist[$domain] = 1;
		}
		
		return $this;
	}
	
	/**
	 * Gets the whitelisted domains.
	 * 
	 * @return Iterator<string>
	 */
	public function getWhitelistedDomains() : Iterator
	{
		return new ArrayIterator(\array_keys($this->_whitelist));
	}
	
	/**
	 * Gets whether the given uri is whitelisted.
	 * 
	 * @param UriInterface $uri
	 * @return boolean
	 */
	public function isWhitelisted(UriInterface $uri) : bool
	{
		$domain = $uri->getHost();
		if(\function_exists('idn_to_ascii'))
		{
			$domain = \idn_to_ascii($domain, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
		}
		
		return isset($this->_whitelist[$domain]);
	}
	
}
