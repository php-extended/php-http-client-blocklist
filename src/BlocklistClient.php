<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-blocklist library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Blocklist\BlocklistInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * BlocklistClient class file.
 * 
 * This class is an implementation of a client which blocks requests based on
 * a list or a resolver that decides whether a domain is suitable to be
 * requested, or needs to be denied.
 * 
 * @author Anastaszor
 */
class BlocklistClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The response factory.
	 * 
	 * @var ResponseFactoryInterface
	 */
	protected ResponseFactoryInterface $_responseFactory;
	
	/**
	 * The blocklist.
	 * 
	 * @var BlocklistInterface
	 */
	protected BlocklistInterface $_blocklist;
	
	/**
	 * The configuration for this blocklist client.
	 * 
	 * @var BlocklistConfiguration
	 */
	protected BlocklistConfiguration $_configuration;
	
	/**
	 * Builds a new BlocklistClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param ResponseFactoryInterface $responseFactory
	 * @param BlocklistInterface $blocklist
	 * @param BlocklistConfiguration $config
	 */
	public function __construct(ClientInterface $client, ResponseFactoryInterface $responseFactory, BlocklistInterface $blocklist, ?BlocklistConfiguration $config = null)
	{
		$this->_client = $client;
		$this->_responseFactory = $responseFactory;
		$this->_blocklist = $blocklist;
		if(null === $config)
		{
			$config = new BlocklistConfiguration();
		}
		$this->_configuration = $config;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if($this->_configuration->isWhitelisted($request->getUri()))
		{
			return $this->_client->sendRequest($request);
		}
		
		if($this->_configuration->hasBlockAllExceptWhitelist())
		{
			return $this->deny($request, 'Block Everything in Whitelist and Absent in Whitelist.');
		}
		
		$result = $this->_blocklist->isUriAllowed($request->getUri());
		if($result->isBlocked())
		{
			return $this->deny($request, $result->getReason());
		}
		
		return $this->_client->sendRequest($request);
	}
	
	/**
	 * Forges a response that denies the request based on the given reason.
	 * 
	 * @param RequestInterface $request
	 * @param string $reason
	 * @return ResponseInterface
	 */
	protected function deny(RequestInterface $request, string $reason) : ResponseInterface
	{
		$response = $this->_responseFactory->createResponse(403, $reason);
		
		try
		{
			$response = $response->withAddedHeader('Cache-Control', 'max-age=0');
			$response = $response->withAddedHeader('Connection', 'close');
			$response = $response->withAddedHeader('Content-Language', 'en');
			$response = $response->withAddedHeader('Content-Length', '0');
			$response = $response->withAddedHeader('Content-Security-Policy', "default-src : 'none'");
			$response = $response->withAddedHeader('Content-Type', 'text/html; charset=utf-8');
			$response = $response->withAddedHeader('Date', \date('D, d M Y H:i:s').' GMT');
			$response = $response->withAddedHeader('Expires', \date('D, d M Y H:i:s').' GMT');
			$response = $response->withAddedHeader('Last-Modified', \date('D, d M Y H:i:s').' GMT');
			$response = $response->withAddedHeader('Pragma', 'no-cache');
			$response = $response->withAddedHeader('Server', __CLASS__);
			$response = $response->withAddedHeader('Status', '403 Forbidden');
			$response = $response->withAddedHeader('X-Content-Type-Options', 'nosniff');
			$response = $response->withAddedHeader('X-Powered-By', 'PHP/'.\PHP_VERSION);
			$response = $response->withAddedHeader('X-Response-Code', '403');
			$response = $response->withAddedHeader('X-Response-Reason', $reason);
			$response = $response->withAddedHeader('X-Target-Uri', $request->getUri()->__toString());
			$response = $response->withAddedHeader('X-XSS-Protection', '1; mode=block');
		}
		catch(InvalidArgumentException $e)
		{
			// nothing to do
		}
		
		return $response;
	}
	
}
